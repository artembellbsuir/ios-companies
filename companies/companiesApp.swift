//
//  companiesApp.swift
//  companies
//
//  Created by artembell on 2/21/21.
//

import SwiftUI

@main
struct companiesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
